<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {

      function __construct(){

            parent::__construct();

            $this->load->model('m_siswa');

      }

     

      public function index(){

            $x['data']=$this->m_siswa->show_siswa();

            $this->load->view('datasiswa',$x);

      }

      function simpan_siswa(){
      	$nama_siswa=$this->input->post('nama_siswa');
        $nis=$this->input->post('nis');
        $jurusan=$this->input->post('jurusan');
        $this->m_siswa->simpan_siswa($nama_siswa,$nis,$jurusan);
        redirect('siswa');
      }

      function edit_siswa(){
      	$id_siswa=$this->input->post('id_siswa');
      	$nama_siswa=$this->input->post('nama_siswa');
        $nis=$this->input->post('nis');
        $jurusan=$this->input->post('jurusan');
        $this->m_siswa->edit_siswa($id_siswa,$nama_siswa,$nis,$jurusan);
        redirect('siswa');
      }

      function hapus_siswa(){
        $id_siswa=$this->input->post('id_siswa');
        $this->m_siswa->hapus_siswa($id_siswa);
        redirect('siswa');
    }

}