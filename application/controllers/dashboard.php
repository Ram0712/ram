<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('admin');
        $this->load->model('m_siswa');
    }

    public function index()
    {
        if($this->admin->logged_id())
        {

            $this->load->view("dashboard");         

        }else{

            //jika session belum terdaftar, maka redirect ke halaman login
            redirect("login");

        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }
    function datasiswa(){

             $x['data']=$this->m_siswa->show_siswa();

            $this->load->view('datasiswa',$x);
        }
        function home(){
            $this->load->view('home');
        }
}