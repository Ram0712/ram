<!DOCTYPE html>
<html>
<head>
    <title> Welcome Back ! </title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" style="background-color: grey">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="#"><h2 class="text-left" style="color: white"> <b>Data Siswa</b> </h2><a>
        </div>
        <!-- <div id="navbar" class="navbar-collapse collapse">
            <div class="navbar-form navbar-right" style="background-color: grey">
                <a href="<?php echo base_url() ?>index.php/dashboard/logout" type="submit" class="btn btn-danger"><i class="fa fa-sign-out"></i> <b style="color: white">Logout</b></a>
            </div> -->
      </div>
    </nav>
<div class="container" style="margin-top: 80px">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group">
              <a href="#" class="list-group-item active" style="text-align: center;background-color: grey;border-color: grey">
                <b>DATA</b>
              </a>
              <a href="<?php echo base_url().'index.php/dashboard/home' ?>" class="list-group-item"><i class="fa fa-home"></i> <b>Home</b> </a>
              <a href="<?php echo base_url().'index.php/dashboard/datasiswa' ?>" class="list-group-item"><i class="fa fa-user"></i> <b>Data Siswa</b> </a>
              <a href="<?php echo base_url().'index.php/dashboard/logout' ?>" class="list-group-item"><i class="fa fa-sign-out" style="color: red"></i> <b>Logout</b> </a>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-home"></i><b> Home </b></h3>
              </div>
              <div class="panel-body"><b>
                Selamat Datang </b><b><?php echo $this->session->userdata("user_nama") ?></b>
              </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>