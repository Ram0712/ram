<!DOCTYPE html>
<html>
<head>
    <title> MENU | SISWA </title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" style="background-color: grey">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="#"><h2 class="text-left" style="color: white"> <b>Data Siswa</b> </h2><a>
        </div>
        <!-- <div id="navbar" class="navbar-collapse collapse">
            <div class="navbar-form navbar-right" style="background-color: grey">
                <a href="<?php echo base_url() ?>index.php/dashboard/logout" type="submit" class="btn btn-danger"><i class="fa fa-sign-out"></i> <b style="color: white">Logout</b></a>
            </div> -->
      </div>
    </nav>
<div class="container" style="margin-top: 80px">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group">
              <a href="#" class="list-group-item active" style="text-align: center;background-color: grey;border-color: grey">
                <b>DATA</b>
              </a>
              <a href="<?php echo base_url().'index.php/dashboard/home' ?>" class="list-group-item"><i class="fa fa-home"></i> <b>Home</b> </a>
              <a href="<?php echo base_url().'index.php/dashboard/datasiswa' ?>" class="list-group-item"><i class="fa fa-user"></i> <b>Data Siswa</b> </a>
              <a href="<?php echo base_url().'index.php/dashboard/logout' ?>" class="list-group-item"><i class="fa fa-sign-out" style="color: red"></i> <b>Logout</b> </a>
            </div>
        </div>
        <div class="col-md-9">
          
              <div class="container">

      <h1>Data <small>Siswa! </small></h1>
 <div class="pull-left"><a class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_add_new"> Add New</a></div>
      <table class="table table-bordered table-striped" id="mydata">

            <thead>

                  <tr>

                        <td><b>ID Siswa</b></td>

                        <td><b>Nama Siswa</b></td>

                        <td><b>NIS</b></td>

                        <td><b>Jurusan<b></td>

                        <td><b>Action</b></td>

                  </tr>

            </thead>
            <tbody>

                  <?php

                        foreach($data->result_array() as $i):

                              $id_siswa=$i['id_siswa'];

                              $nama_siswa=$i['nama_siswa'];

                              $nis=$i['nis'];

                              $jurusan=$i['jurusan'];

                  ?>

                  <tr>

                        <td><?php echo $id_siswa; ?> </td>

                        <td><?php echo $nama_siswa;?> </td>

                        <td><?php echo $nis;?></td>

                        <td><?php echo $jurusan;?> </td>

                        <td style="width: 120px">
                          <a class="btn btn-xs btn-info" data-toggle="modal" data-target="#modal_edit<?php echo $id_siswa;?>"><b> Edit </b></a>
                          <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal_hapus<?php echo $id_siswa;?>"><b> Hapus </b></a>
                        </td>
                        
                        </tr>


                  <?php endforeach;?>

            </tbody>
          </table>
        </div>
        <!-- ============ MODAL ADD BARANG =============== -->
         <div class="modal fade" id="modal_add_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">Add New Siswa</h3>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url().'index.php/siswa/simpan_siswa'?>">
                <div class="modal-body">
 
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama Siswa</label>
                        <div class="col-xs-8">
                            <input name="nama_siswa" class="form-control" type="text" placeholder="Nama Siswa" required>
                        </div>
                    </div>
 
                    <div class="form-group">
                        <label class="control-label col-xs-3" >NIS</label>
                        <div class="col-xs-8">
                            <input name="nis" class="form-control" type="text" placeholder="NIS" required>
                        </div>
                    </div>
 
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Jurusan</label>
                        <div class="col-xs-8">
                             <select name="jurusan" class="form-control" required>
                                <option value="">-PILIH-</option>
                                <option value="RPL">RPL</option>
                                <option value="TKJ">TKJ</option>
                                <option value="GMTK">GMTK</option>
                                <option value="GEO">GEO</option>
                                <option value="TABUS">TABUS</option>
                                <option value="MEKA">MEKA</option>
                             </select>
                        </div>
                    </div>
 
                </div>
 
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info">Simpan</button>
                </div>
            </form>
            </div>
            </div>
        </div>
        <!-- END -->
        <!-- ============ EDIT =============== -->
    <?php
        foreach($data->result_array() as $i):
            $id_siswa=$i['id_siswa'];
            $nama_siswa=$i['nama_siswa'];
            $nis=$i['nis'];
            $jurusan=$i['jurusan'];
        ?>
        <div class="modal fade" id="modal_edit<?php echo $id_siswa;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">Edit Siswa</h3>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url().'index.php/siswa/edit_siswa'?>">
                <div class="modal-body">
 
                    <div class="form-group">
                        <label class="control-label col-xs-3" >ID Siswa</label>
                        <div class="col-xs-8">
                            <input name="id_siswa" value="<?php echo $id_siswa;?>" class="form-control" type="text" placeholder="Id Siswa" readonly>
                        </div>
                    </div>
 
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama Siswa</label>
                        <div class="col-xs-8">
                            <input name="nama_siswa" value="<?php echo $nama_siswa;?>" class="form-control" type="text" placeholder="Nama Siswa" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >NIS</label>
                        <div class="col-xs-8">
                            <input name="nis" value="<?php echo $nis;?>" class="form-control" type="text" placeholder="NIS" readonly>
                        </div>
                    </div>
 
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Jurusan</label>
                        <div class="col-xs-8">
                             <select name="jurusan" class="form-control" required>
                                <option value="">-PILIH-</option>
                                <?php if($jurusan=='RPL'):?>
                                    <option value="RPL" selected>RPL</option>
                                    <option value="TKJ">TKJ</option>
                                    <option value="GMTK">GMTK</option>
                                    <option value="GEO">GEO</option>
                                    <option value="TABUS">TABUS</option>
                                    <option value="MEKA">MEKA</option>
                                <?php elseif($jurusan=='TKJ'):?>
                                    <option value="RPL">RPL</option>
                                    <option value="TKJ" selected>TKJ</option>
                                    <option value="GMTK">GMTK</option>
                                    <option value="GEO">GEO</option>
                                    <option value="TABUS">TABUS</option>
                                    <option value="MEKA">MEKA</option>
                                <?php elseif($jurusan=='GMTK'):?>
                                    <option value="RPL">RPL</option>
                                    <option value="TKJ">TKJ</option>
                                    <option value="GMTK" selected>GMTK</option>
                                    <option value="GEO">GEO</option>
                                    <option value="TABUS">TABUS</option>
                                    <option value="MEKA">MEKA</option>
                                <?php elseif($jurusan=='GEO'):?>
                                    <option value="RPL">RPL</option>
                                    <option value="TKJ">TKJ</option>
                                    <option value="GMTK">GMTK</option>
                                    <option value="GEO" selected>GEO</option>
                                    <option value="TABUS">TABUS</option>
                                    <option value="MEKA">MEKA</option>
                                <?php elseif($jurusan=='TABUS'):?>
                                    <option value="RPL">RPL</option>
                                    <option value="TKJ">TKJ</option>
                                    <option value="GMTK">GMTK</option>
                                    <option value="GEO">GEO</option>
                                    <option value="TABUS" selected>TABUS</option>
                                    <option value="MEKA">MEKA</option>
                                <?php else:?>
                                    <option value="RPL">RPL</option>
                                    <option value="TKJ">TKJ</option>
                                    <option value="GMTK">GMTK</option>
                                    <option value="GEO">GEO</option>
                                    <option value="TABUS">TABUS</option>
                                    <option value="MEKA" selected>MEKA</option>
                                <?php endif;?>
                             </select>
                        </div>
                    </div>
 
                </div>
 
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info">Update</button>
                </div>
            </form>
            </div>
            </div>
        </div>
 
    <?php endforeach;?>
    <!--END-->

    <!-- ============  HAPUS  =============== -->
     <?php
        foreach($data->result_array() as $i):
            $id_siswa=$i['id_siswa'];
            $nama_siswa=$i['nama_siswa'];
            $nis=$i['nis'];
            $jurusan=$i['jurusan'];
        ?>
        <div class="modal fade" id="modal_hapus<?php echo $id_siswa;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
               <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h3 class="modal-title" id="myModalLabel">Hapus Siswa</h3>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url().'index.php/siswa/hapus_siswa'?>">
                <div class="modal-body">
                    <p>Anda yakin mau menghapus <b><?php echo $nama_siswa;?>?</b></p>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id_siswa" value="<?php echo $id_siswa;?>">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-danger">Hapus</button>
                </div>
            </form>
            </div>
            </div>
        </div>
    <?php endforeach;?>
    <!--END   -->


        <script>
          $(document).ready(function(){
            $('#mydata').DataTable();
          });
        </script>
    </div>
</div>


<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>